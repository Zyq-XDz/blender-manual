.. index:: Extensions

.. Mark as "orphan" until extensions is out of beta.

:orphan:

##########
Extensions
##########

.. important::

   This feature is only available experimentally in
   `daily builds <https://builder.blender.org/download/daily/>`__ of
   `Blender 4.2 <https://projects.blender.org/blender/blender/milestone/19>`__.
   Please enable "Extensions" on the :doc:`Experimental Preferences </editors/preferences/experimental>`
   to help testing it.

Extensions are **add-ons** or **themes** used to extend the core functionality of Blender.
They are shared in online platforms, and can be installed and updated from within Blender.

The official extensions platform for the Blender project is `extensions.blender.org <https://extensions.blender.org>`__.
Other third party sites can also be supported, as long as they follow the Extensions Platform specification.

.. seealso::

   For the extension settings, and how to manage them, refer to the
   :doc:`User Preferences </editors/preferences/extensions>`.

.. toctree::
   :maxdepth: 1

   Getting started <getting_started.rst>
   Compatible licenses <licenses.rst>
   Supported tags <tags.rst>
   Add-ons <addons.rst>